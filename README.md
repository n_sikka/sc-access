# SC-ACCESS
UI/UX created for an app that faacilitates natural language searching in public databases


## Quick Start
Install Node.js and then:

- git clone https://gitlab.com/n_sikka/sc-access.git
- cd sc-access
- sudo npm -g install gulp bower
- npm install
- bower install
- gulp serve

App will be live on localhost:3000

## Technologies
- AngularJS
- Bootstrap
- Ui-Router
- ui-bootstrap

# Tools used
- Gulp
- NPM
- Bower
- LiveReload
